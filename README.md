# PocketSchool_WeCchat_MiniApp

#### 项目介绍
项目负责人 全栈开发 涵盖用户调研、产品设计、前端、后端和后期维护。组件化编写，后端运用 Java 并使用 nginx 进行负载均衡。

#### 软件架构
HTML5 css3 JavaScript java nginx


#### 安装教程

导入项目到微信小程序开发工具  服务器端使用Tomcat执行，然后nginx负载均衡

#### 使用说明

1. 使用前注意代理网络
2. 跨域访问
3. 验证SSL证书

#### 参与贡献

1. 叶武（yexuan）
2. 天津知境网络科技
